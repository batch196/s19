console.log("hello");

// conditional statement allows us to perform
//tasks based on a condition

let num1 = 25;
/*if(condition){
	task/code to perform
}*/
if(num1 === 0){
	console.log("the value is Zero");
} else {
	console.log("the value is not zero");
}

let city = "new york";
if(city === "new jersey"){
	console.log("welcome to new jersey");
} 

if(city === "new jersey"){
	console.log("welcome to new jersey");
} else {
	console.log("you are in new york");
} 

 if(num1 < 20){
 	console.log("num1's value is less than 20");
 } else {
 	console.log("num1's value is more than 20");
 }

function cityChecker(city){
	if(city === "new york"){
		console.log("welcome to new york");
	} else {
		console.log("you are in jersey");
	}
}

cityChecker("new york");
cityChecker("new jersey");

// else if
// allows us to execute code/tesk if the condition are
// not met or false

let city2 = "manila";

if(city2 === "new york"){
	console.log("welcome to new york");
} else if(city2 === "manila"){
	console.log("welcome to manila")
} else {
	console.log("i dont know where you are");
}

let role = "admin";

if(role === "developer"){
	console.log("welcome back, Dev!");
} else if(role === "admin") {
	console.log("role provided is invalid");
} else {
	console.log("Role is out of bounds");
}

function determineWindIntensity(windspeed){
	if(windspeed < 30){
		return "not a typhoon";
	} else if(windspeed <= 61){
		return "tropical depression";
	} else if(windspeed >= 62 && windspeed <= 88)
	{	return "troppical storm";

	} else if(windspeed >= 89 && windspeed <= 117){
		return "severe storm";
	}	else {
		return "typhoon detected"
	}
}
let typhoonMessage1 = determineWindIntensity(29);
let typhoonMessage2 = determineWindIntensity(62);
let typhoonMessage3 = determineWindIntensity(61);
let typhoonMessage4 = determineWindIntensity(88);

let typhoonMessage5 = determineWindIntensity(117);
let typhoonMessage6 = determineWindIntensity(120);


console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);

//truthy and false values
//in js there are values considered "truthy" which means in a boolean context, like determining an if condition. it is considered true
// samples
// 1. true
//2.
if(1){
	console.log("1 is a truthy");
}
//3.
if([]){
	// even empty array is empty, it already exists
	console.log("[] emplty array is truthy");
}
// falsy values are false in a boolean context

// 1. false
// 2.
if(0){
	console.log("0 is falsy");
}
// 3. 
if(undefined){
	console.log("undefined is not falsy");
} else {
	console.log("undefined is falsy")
}

// conditional Ternary operator
// is used as a shorter alternative to if else statement
// it is also ablbe to implicitly retun a value. meaning it does not have to use the return a value
// syntax: (condition) ? ifTrue : ifFalse;

let age = 17;
let result = age < 18 ? "you are underaged" : "legal age";
console.log(result);

/*switch statement
evaluate an expression and match the expression to a case clause*/
// an expression will be compared against different cases then will be able to run conde if the expression being evaluated matches a case
// it is used alternatiely from if else statement. however, if-else statements provides more complexity in its condition
// .toLowerCase method is a built in js method which converts a string to lowercase
let day = prompt("what day of the week today?").toLowerCase();
console.log(day);

// switch statement to evaluate the current date and show a message to tell the user the color of the day

switch(day){
	case 'monday':
		console.log("color of the day is red");
	break;
	case 'tuesday':
		console.log("the color of the day is orange");
	break;
	case 'wednesday':
		console.log("the color of the day is yellow");
	default:
		console.log("Please enter a valid day");
	case 'Thursday':
	console.log("color of the day is black");
	break;
	case 'friday':
	console.log('color of the day is green');
	break;
	case 'saturday':
	console.log('color of the days is maroon');
	break;
	case 'sunday':
	console.log('color of the days is blue');
	break;
}
// try-catch-finally
// used to catch errors, display and inform about the error
//also used to produce our own error message

try{
	alert(determineWindIntensity(50))
//the error is passed into the catch statement
} catch (error){
	//with the use of typeof keyword we will be able to return the string that contains error
	console.log(error.message);
} finally {
	//finally statement will run regardless of the success or failure of the try statement
}	alert("Intensity updates will show on next alert");

console.log("Will be continue to the next code?");



